import java.util.Scanner;
public class Application{
	public static void main(String[]args){
		Scanner keyboard = new Scanner(System.in);
		Student[] students = new Student[4];
		
		for(int i = 0; i < students.length; i++){
			students[i] = new Student("Bryan", 60); //making a new student
			
			System.out.println("What is your name?");
			String nameOfStudent = keyboard.next();
			//System.out.println(students[0].getName());
				//students[i].setName(nameOfStudent);
			//System.out.println(students[0].getName());
			
			System.out.println("What is your grade?");
			int grade = keyboard.nextInt();
				//students[i].setGrade(grade);
			
			System.out.println("How many classes do you have?");
			int classes = keyboard.nextInt();
				//students[i].setNumberOfClasses(classes);
		}
		
		//p1 q2
		/*
		for(int i = 0; i < students.length; i++){
			System.out.println(students[i].amountLearnt);
		}
		*/
		
		//p1 q5
		/*
		students[3].learn();
		students[3].learn();
		*/
		
		//p1 q6
		/*
		for(int i = 0; i < students.length; i++){
			System.out.println(students[i].amountLearnt);
		}
		*/
		
		//p2 q2
		/*
		System.out.println("Input amount studied: ");
		int amountStudied = keyboard.nextInt();
		//p2 q3
		students[3].learn(amountStudied);
		*/
		
		/*
		//p2 q4
		for(int i = 0; i < students.length; i++){
			System.out.println(students[i].amountLearnt);
		}
		*/
		
		//p2.5
		System.out.println("Input amount studied a second time: ");
		students[1].learn(keyboard.nextInt());
		
		System.out.println("Input amount studied a third time: ");
		students[1].learn(keyboard.nextInt());
		
		//p3 q2
		System.out.println(students[0].getName());
		System.out.println(students[0].getGrade());
		System.out.println(students[0].getNumberOfClasses());
		System.out.println(students[0].getAmountLearnt());
		
		
		//lab 4 part 2 (constructor part of lab) starts here
		Student newStudent = new Student("Bryan", 60);
		System.out.println(newStudent.getName());
		System.out.println(newStudent.getGrade());
		System.out.println(newStudent.getNumberOfClasses());
		System.out.println(newStudent.getAmountLearnt());
		
		newStudent.setNumberOfClasses(0);
		System.out.println(newStudent.getNumberOfClasses());
	}
}
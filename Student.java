public class Student{
	private String name;
	private int grade;
	private int numberOfClasses;
	private int amountLearnt;
	
	public void goToSchool(){
		System.out.println("Damn its cold outside. I'm getting fed up of having to go to all " + numberOfClasses + " of my classes."); 
	}
	public void complainAboutTeachers(){
		System.out.println("I only have a " + grade + " in their class. It's so annoying.");
	}
	
	public void learn(int amountStudied){
		System.out.println(amountLearnt); //before
		if(amountStudied > 0){
			amountLearnt += amountStudied;
		}	
		System.out.println(amountLearnt); //after
	}
	
	//p3 q1
	public String getName(){
		return this.name;
	}
	/*public void setName(String newName){
		this.name = newName;
	}*/
	
	public int getGrade(){
		return this.grade;
	}
	/*public void setGrade(int newGrade){
		this.grade = newGrade;
	}*/
	
	public int getNumberOfClasses(){
		return this.numberOfClasses;
	}
	public void setNumberOfClasses(int newNumberOfClasses){
		this.numberOfClasses = newNumberOfClasses;
	}
	
	public int getAmountLearnt(){
		return this.amountLearnt;
	}
	public void setAmountLearnt(int newAmountLearnt){
		this.amountLearnt = newAmountLearnt;
	}
	
	//constructor
	public Student(String name, int grade){
		this.name = name;
		this.grade = grade;
		this.numberOfClasses = 7;
		this.amountLearnt = 0;
	}
}
